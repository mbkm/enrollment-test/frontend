Buatlah suatu project berbasis web sederhana dengan tema "To-Do List",  
project ini berisi dua halaman :  
- Tabel tanggal Per Bulan
- Detail Tanggal

#### Tabel Agenda Per Bulan
Tabel Agenda Per Bulan memiliki fitur :
1. Memilih bulan dan tahun
2. Menampilkan daftar tanggal dalam bulan tersebut

#### Detail Tanggal
Detail Tanggal memiliki fitur :
1. Menampilkan daftar seluruh agenda sesuai dengan tanggal yang dipilih
2. Menambah suatu agenda ke dalam To-Do List pada tanggal yang dipilih
3. Mengubah suatu agenda di dalam To-Do List pada tanggal yang dipilih
4. Menghapus suatu agenda di dalam To-Do List pada tanggal yang dipilih

### Catatan
Detail yang perlu diperhatikan :
- Untuk masuk ke dalam ```Detail Tanggal```, pilih tanggal yang ada di halaman ```Tabel Agenda Per Bulan```.
- Untuk Tabel pada halaman ```Tabel Agenda Per Bulan```, bisa dibuat dalam bentuk tabel biasa.
- Diharuskan untuk menggunakan Vue.js atau Client-Side Rendering.
- Tidak perlu menyimpan data di database, cukup di vuex atau localStorage.
- Usahakan untuk selalu memberikan dokumentasi berupa komentar pada bagian-bagian yang penting.
- Usahakan untuk membuat struktur folder dan file dengan rapi.
